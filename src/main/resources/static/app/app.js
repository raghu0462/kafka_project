//var app = angular.module('app', []);

var app = angular.module('app', ['ui.bootstrap', 'app.controllers', 'app.services']);

app.constant("CONSTANTS", {	
	getAllSourceQueues : "/kafka",
	getAllDestinationQueues : "api/routes/destination",
	getAllUsers : "/users",
	saveAllRoutes : "/routesadd",
	searchUsersNames : "/users/search"
});

   