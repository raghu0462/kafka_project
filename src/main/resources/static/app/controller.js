var app = angular.module('app', []);
app.controller("RouteController", [ "$scope", "RouteService",
		function($scope, RouteService,$http, $location) {
		
		$scope.sourceQueues =  RouteService.getAllSourceQueues().then(function(value) {
			$scope.sourceQueues = value.data;
		}, function(reason) {
			console.log("error occured");
		}, function(value) {
			console.log("no callback");
		});
		
		$scope.destinationQueues =  RouteService.getAllDestinationQueues().then(function(value) {
			$scope.destinationQueues = value.data;
		}, function(reason) {
			console.log("error occured");
		}, function(value) {
			console.log("no callback");
		});
     
		
		
			
			// be used to decide for showing PostResults   
			$scope.postDivAvailable = false;
		
			
			$scope.addRouteFunction = function(){
			
				// post URL
				var url = "http://localhost:58678/kafka";
				
				// prepare headers for posting
				var config = {
		                headers : {
		                    'Content-Type': 'application/json',
		                    'Accept': 'application/json'
		                    // 'Accept': 'text/plain'
		                }
		        }
				
				
				// prepare data for post messages
				var data = {
		            source: $scope.route.SourceSelected,
		            routeno: $scope.route.RouteVal,
		            desination: $scope.route.destinSelected
		        };
				
				// do posting
				$http.post(url, data, config).then(function (response) {
					$scope.postDivAvailable = true;
					$scope.postCust =  response.data;
				}, function error(response) {
					$scope.postResultMessage = "Error Status: " +  response.statusText;
				});
				
				// reset data of form after posting
				$scope.route.SourceSelected = '';
				$scope.route.RouteVal = '';
				$scope.route.destinSelected = '';
			}

	
		
		} ]);