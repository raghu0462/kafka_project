angular.module('app.services', []).factory('RouteService',
		[ "$http", "CONSTANTS", function( $http, CONSTANTS) {
			
			var service = {};
			
			service.getAllSourceQueues = function() {
				var url = CONSTANTS.getAllSourceQueues;
				return $http.get(url);
			}
	          service.getAllDestinationQueues = function() {
				var url2 = CONSTANTS.getAllDestinationQueues;
				return $http.get(url2);
			}
	
			
			return service;
			
		} ]);