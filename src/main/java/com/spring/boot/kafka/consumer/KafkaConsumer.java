package com.spring.boot.kafka.consumer;

import org.springframework.kafka.annotation.KafkaListener;

import org.springframework.stereotype.Service;

import com.spring.boot.kafka.model.User;

@Service
public class KafkaConsumer {

	@KafkaListener(topics = "Kafka_Example2", groupId = "group_id")
	public void consume(String message) {
		System.out.println("Consumed message:" + message);
	}

	@KafkaListener(topics = "Kafka_Example2_json", groupId = "group_json", containerFactory = "userkafkaListenerFactory")
	public void consumeJson(User user) {
		System.out.println("Consumed Json message:" + user);
	}

}
