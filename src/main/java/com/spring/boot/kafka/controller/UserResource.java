package com.spring.boot.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.boot.kafka.model.User;

@RestController
@RequestMapping("/kafka")
public class UserResource {
/*	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate1;

	private static final String TOPIC1 = "Kafka_Example2";
	

	@GetMapping("/publish/{message}")
	public String post1(@PathVariable("message") final String message) {

		kafkaTemplate1.send(TOPIC1, message);

		return "Published Successfully on " + TOPIC1;
	}
	*/

	@Autowired
	private KafkaTemplate<String, User> kafkaTemplate;

	private static final String TOPIC = "Kafka_Example2";
	

	@GetMapping("/publish/{name}")
	public String post(@PathVariable("name") final String name) {

		kafkaTemplate.send(TOPIC, new User(name, "Technology"));

		return "Published" + name + " Successfully on " + TOPIC;
	}

	@GetMapping
	public String status() {
		return "Available";
	}

}
