package com.spring.boot.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootKafkaMain {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootKafkaMain.class, args);
	}

}
